FROM node:16 as node
WORKDIR /app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
RUN ng build

FROM nginx:alpine
COPY --from=node /app/dist/mds-dds-exam-angular /usr/share/nginx/html